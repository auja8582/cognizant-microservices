package com.cognizant.controller;

import com.cognizant.TaskPersistenceApplication;
import com.cognizant.model.TaskFormModel;
import com.cognizant.persistence.model.Task;
import com.cognizant.persistence.repository.TaskRepository;
import com.cognizant.persistence.service.TaskPersistenceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskPersistenceController.class)
@ContextConfiguration(classes = {TaskPersistenceApplication.class})
public class TaskPersistenceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskPersistenceController taskPersistenceController;

    @MockBean
    private TaskPersistenceService taskPersistenceService;

    @MockBean
    private TaskFormModel taskFormModel;

    @Test
    public void testTaskPersistenceControllerReturnsOk() throws Exception {
        doReturn(1L).when(taskPersistenceService).saveTask(taskFormModel);
        mockMvc.perform(post("/createtask")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"name\": \"name\",\n" +
                        "    \"timeSpent\": \"123cls456878uhjk\",\n" +
                        "    \"group\": \"group1\",\n" +
                        "    \"assignee\": \"s04545ty\",\n" +
                        "    \"subTasks\": [\n" +
                        "        {\n" +
                        "            \"name\": \"test engine\",\n" +
                        "            \"isTasFinished\": \"who knows\",\n" +
                        "            \"description\": \"nothing to see\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"test wheels\",\n" +
                        "            \"isTasFinished\": \"154 minutes\",\n" +
                        "            \"description\": \"four wheels\"\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"isTasFinished\": \"no one knows\"\n" +
                        "}")).andExpect(status().isOk());
    }

    @Test
    public void testTaskPersistenceControllerInvokesTaskpersistenceService() throws Exception {
        doReturn(1L).when(taskPersistenceService).saveTask(taskFormModel);
        mockMvc.perform(post("/createtask")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"name\": \"name\",\n" +
                        "    \"timeSpent\": \"123cls456878uhjk\",\n" +
                        "    \"group\": \"group1\",\n" +
                        "    \"assignee\": \"s04545ty\",\n" +
                        "    \"subTasks\": [\n" +
                        "        {\n" +
                        "            \"name\": \"test engine\",\n" +
                        "            \"isTasFinished\": \"who knows\",\n" +
                        "            \"description\": \"nothing to see\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"test wheels\",\n" +
                        "            \"isTasFinished\": \"154 minutes\",\n" +
                        "            \"description\": \"four wheels\"\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"isTasFinished\": \"no one knows\"\n" +
                        "}"));
        verify(taskPersistenceService, times(1))
                .saveTask(any(TaskFormModel.class));
    }
}