package com.cognizant;

import com.cognizant.controller.TaskPersistenceController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class TaskPersistenceApplicationTest {
    @Autowired
    private TaskPersistenceController taskPersistenceController;

    @Test
    public void testContextLoads() throws Exception {
        assertThat(taskPersistenceController).isNotNull();
    }
}