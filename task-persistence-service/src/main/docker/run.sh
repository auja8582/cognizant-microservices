#!/bin/sh

#mysql waiting script was taken from
#https://cweiske.de/tagebuch/docker-mysql-available.htm
echo "********************************************************"
echo "Waiting for the database server to start on port $DATABASESERVER_PORT"
echo "********************************************************"
maxcounter=45

counter=1
while ! mysql --host="$DATABASE_URL" --protocol TCP -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" -e "show databases;" > /dev/null 2>&1; do
    sleep 1
    counter=`expr $counter + 1`
    if [ $counter -gt $maxcounter ]; then
        >&2 echo "We have been waiting for MySQL too long already; failing."
        exit 1
    fi;
done

echo "********************************************************"
echo "Starting Task Persistence Service"
echo "Using Profile: $PROFILE"
echo "********************************************************"
java -Djava.security.egd=file:/dev/./urandom -Dserver.port=$SERVER_PORT   \
     -Dspring.profiles.active=$PROFILE                                    \
     -jar /usr/local/taskpersistenceservice/@project.build.finalName@.jar
