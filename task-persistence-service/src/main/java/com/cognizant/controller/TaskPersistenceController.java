package com.cognizant.controller;

import com.cognizant.model.TaskFormModel;
import com.cognizant.persistence.service.TaskPersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskPersistenceController {

    private TaskPersistenceService taskPersistenceService;

    @Autowired
    public TaskPersistenceController(TaskPersistenceService taskPersistenceService) {
        this.taskPersistenceService = taskPersistenceService;
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "/createtask")
    public ResponseEntity<?> createTask(@RequestBody TaskFormModel
                                                taskFormModel) {
        try {
            Long taskId = taskPersistenceService.saveTask(taskFormModel);
            return ResponseEntity.ok(taskId);
        } catch (Exception e){
            return ResponseEntity.status(500).build();
        }
    }

}