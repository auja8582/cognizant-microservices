package com.cognizant.persistence.service;

import com.cognizant.model.TaskFormModel;
import com.cognizant.persistence.model.SubTask;
import com.cognizant.persistence.model.Task;
import com.cognizant.persistence.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class TaskPersistenceService {

    private TaskRepository taskRepository;

    @Autowired
    public TaskPersistenceService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * TODO: add aspectj weaving and refactor to private methods
     */
    @Transactional
    public Long saveTask(TaskFormModel taskFormModel) {
        Task task = new Task();
        task.setAssignee(taskFormModel.getAssignee());
        task.setIsTasFinished(taskFormModel.getIsTasFinished());
        task.setName(taskFormModel.getName());
        taskFormModel.getSubTasks().forEach((s)->{
            SubTask subTask = new SubTask();
            subTask.setDescription(s.getDescription());
            subTask.setIsTasFinished(s.getIsTasFinished());
            subTask.setName(s.getName());
            task.addSubTask(subTask);
        });
        task.setTaskGroup(taskFormModel.getGroup());
        task.setTimeSpent(taskFormModel.getTimeSpent());
        return taskRepository.save(task).getId();
    }

}
