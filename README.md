# README #

This README would normally document whatever steps are necessary to get your application up and running.

The aftermath:

Applied for https://bitbucket.org/auja8582/cognizant-microservices/downloads/cognizant_job_ad.txt.
And as a pre interview task had to do https://bitbucket.org/auja8582/cognizant-microservices/downloads/JAVA_task_short_2019.pdf

[comment]: <> (GOT AN ANWSER THAT MY APPLICATION WAS REJECTED, BECAUSE I'M TECHNICALLY STRONG, BUT THEY HAD TO CHERRY PICK A RANDOM NONSENSE OUT OF THEIR ASS TO REJECT THIS APPLICATION.)

### What is this repository for? ###

* This repo was set up for a pre interview task given by unkowns from Cognizant 
* Original task can be found (https://bitbucket.org/auja8582/cognizant-microservices/downloads/JAVA_task_short_2019.pdf)

### How do I get set up? ###

# SETUP OVERVIEW #

* Change Docker parameters in all pom files.
* Compile the project
* Execute docker-compose.yml script
* Wait till project comes up in docker
* Navigate to `http://[ip address of docker's virtual machine]:5555/swagger-ui.html`
* Try rest endpoints

## Change Docker parameters in all pom files ##

All *.pom files in the project have a docker plugin called [com.spotify] (without brackets)
Replace all entries of 

        <docker.host.url>https://192.168.99.100:2376</docker.host.url>
        <docker.cert.path.location>/Users/administratorius/.docker/machine/machines/default</docker.cert.path.location>
to

        <docker.host.url>https://[ip address of docker machine:[port on which docker listens for connections]</docker.host.url>
        <docker.cert.path.location>[path to docker certs]</docker.cert.path.location>

more info

on how to set up spotify plugin on 

    https://github.com/spotify/docker-maven-plugin
    https://docs.docker.com/machine/reference/regenerate-certs/

## Compile the project ##

Go to project parent folder and initiate ***mvn clean install docker:build*** or use IDEs
such as Eclipse or Intellij for that matter. (Or just manually build Docker images and put
them into docker machine).

So, after the command finishes all docker files and other dependencies should be sent to docker machine

## Execute docker-compose.yml script ##

From project's parent folder (using Docker Toolbox in this case) navigate to

 [projects parent directory]/docker/common/docker-compose.yml
 
 execute ***docker-compose up***
 
 wait till all services are loaded (~3 min on a slow machine)
 
 ## Navigate to `http://[ip address of docker's virtual machine]:5555/swagger-ui.html` ##
 
 # ADDITIONAL NOTES #
 
 DO REMEMBER THAT WINDOWS MACHINES USES CRLF ENDINGS...
 
 WHILE LINUX USES LF.
 
 MAKE SURE THAT YOU PRESERVE LINE ENDINGS AND USE LF!!!!!!!!
 
 Not everything was done as to this task I've decided to dedicate ~2 hours after reading the task itself.
 
 Rationale on why can be found:
 
    https://bitbucket.org/auja8582/cognizant-microservices/downloads/JAVA_task_short_2019_rationale.pdf
 
 
 Process orchestration could've been better.
 
 Tests could've been better.
 
 Swagger could've been better.
 
 The system is only exposing endpoints and has no GUI.