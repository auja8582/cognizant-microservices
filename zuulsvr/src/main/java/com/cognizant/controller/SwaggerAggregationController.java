package com.cognizant.controller;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Primary
public class SwaggerAggregationController implements
        SwaggerResourcesProvider {

    private final Map<String, String> services =
            Map.of("task-persistence-service",
                    "taskpersistenceservice/v2/api-docs",
                    "task-removal-service",
                    "taskremovalservice/v2/api-docs",
                    "task-retrieval-service",
                    "taskretrievalservice/v2/api-docs",
                    "task-update-service",
                    "taskupdateservice/v2/api-docs");

    private final String API_VERSION = "/api/v1/";
    private final String SWAGGER_VERSION = "2.0";

    private SwaggerResource makeSwaggerResource(Map.Entry<String, String> entry,
                                                boolean attachApiVersion){
        SwaggerResource service = new SwaggerResource();
        service.setName(entry.getKey());

        if (attachApiVersion) {
            service.setLocation(API_VERSION + entry.getValue());
        } else {
            service.setLocation(entry.getValue());
        }
        service.setSwaggerVersion(SWAGGER_VERSION);
        return service;
    }

    @Override
    public List<SwaggerResource> get() {
        return services
                .entrySet()
                .stream()
                .map((k) -> makeSwaggerResource(k, true))
                .collect(Collectors.toList());
    }
}

