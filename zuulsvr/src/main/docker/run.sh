#!/bin/sh

echo "********************************************************"
echo "ZuulService waiting for the eureka server to start on port $EUREKASERVER_PORT"
echo "********************************************************"
while ! `nc -z eurekaserver  $EUREKASERVER_PORT`; do sleep 3; done
echo "******* ZuulService reporting that Eureka Server has started"

echo "********************************************************"
echo "Starting Zuul Service on port $SERVER_PORT"
echo "********************************************************"
java -Djava.security.egd=file:/dev/./urandom -Dserver.port=$SERVER_PORT   \
     -Deureka.client.serviceUrl.defaultZone=$EUREKASERVER_URI   \
     -Dspring.profiles.active=$PROFILE                          \
     -jar -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:11112 \
     /usr/local/zuulserver/@project.build.finalName@.jar
