package com.cognizant.persistence.service;

import com.cognizant.persistence.exception.NoTaskObjectFoundException;
import com.cognizant.persistence.model.Task;
import com.cognizant.persistence.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class TaskRemovalService {

    @Autowired
    private TaskRepository taskRepository;

    /**
     * TODO: add aspectj weaving and refactor to private methods
     */
    @Transactional(rollbackFor = NoTaskObjectFoundException.class)
    public void removeTask(Long taskId) throws NoTaskObjectFoundException {
        Task task = taskRepository.findOneById(taskId)
                .orElseThrow(NoTaskObjectFoundException::new);
        taskRepository.delete(task);
    }
}
