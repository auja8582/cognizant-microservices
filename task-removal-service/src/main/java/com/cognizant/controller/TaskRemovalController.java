package com.cognizant.controller;

import com.cognizant.persistence.exception.NoTaskObjectFoundException;
import com.cognizant.persistence.service.TaskRemovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TaskRemovalController {

    @Autowired
    private TaskRemovalService taskRemovalService;

    @Autowired
    public TaskRemovalController(TaskRemovalService taskRemovalService) {
        this.taskRemovalService = taskRemovalService;
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping(value = "/removetask/{taskId}")
    public ResponseEntity<?> removeTask(@PathVariable Long taskId) {
        try {
            taskRemovalService.removeTask(taskId);
            return ResponseEntity.ok().build();
        } catch (NoTaskObjectFoundException e){
            return ResponseEntity.notFound().build();
        } catch (Exception e){
            return ResponseEntity.status(500).build();
        }
    }

}