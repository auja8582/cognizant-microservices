package com.cognizant.controller;

import com.cognizant.TaskRemovalApplication;
import com.cognizant.persistence.service.TaskRemovalService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskRemovalController.class)
@ContextConfiguration(classes = {TaskRemovalApplication.class})
class TaskRemovalControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskRemovalController taskRemovalController;

    @MockBean
    private TaskRemovalService taskRemovalService;

    @Test
    public void testTaskRemovalControllerReturnsOk() throws Exception {
        mockMvc.perform(delete("/removetask/1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    public void testTaskRemovalControllerInvokesTaskRemovalService() throws Exception {
        doNothing().when(taskRemovalService).removeTask(1L);
        mockMvc.perform(delete("/removetask/1"));
        verify(taskRemovalService, times(1))
                .removeTask(any(Long.class));
    }
}