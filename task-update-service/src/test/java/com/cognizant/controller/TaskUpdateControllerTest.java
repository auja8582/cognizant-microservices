package com.cognizant.controller;

import com.cognizant.TaskUpdateApplication;
import com.cognizant.model.TaskFormModel;
import com.cognizant.persistence.service.TaskUpdateService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskUpdateController.class)
@ContextConfiguration(classes = {TaskUpdateApplication.class})
class TaskUpdateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskUpdateController taskUpdateController;

    @MockBean
    private TaskFormModel taskFormModel;

    @MockBean
    private TaskUpdateService taskUpdateService;

    @Test
    public void testTaskUpdateControllerReturnsOk() throws Exception {
        doNothing().when(taskUpdateService).updateTask(taskFormModel, 1L);
        mockMvc.perform(put("/updatetask/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"name\": \"name\",\n" +
                        "    \"timeSpent\": \"123cls456878uhjk\",\n" +
                        "    \"group\": \"group1\",\n" +
                        "    \"assignee\": \"s04545ty\",\n" +
                        "    \"subTasks\": [\n" +
                        "        {\n" +
                        "            \"name\": \"test engine\",\n" +
                        "            \"isTasFinished\": \"who knows\",\n" +
                        "            \"description\": \"nothing to see\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"test wheels\",\n" +
                        "            \"isTasFinished\": \"154 minutes\",\n" +
                        "            \"description\": \"four wheels\"\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"isTasFinished\": \"no one knows\"\n" +
                        "}")
        ).andExpect(status().isOk());
    }

    @Test
    public void testTaskUpdateControllerInvokesTaskUpdateService() throws Exception {
        doNothing().when(taskUpdateService).updateTask(taskFormModel, 1L);
        mockMvc.perform(put("/updatetask/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"name\": \"name\",\n" +
                        "    \"timeSpent\": \"123cls456878uhjk\",\n" +
                        "    \"group\": \"group1\",\n" +
                        "    \"assignee\": \"s04545ty\",\n" +
                        "    \"subTasks\": [\n" +
                        "        {\n" +
                        "            \"name\": \"test engine\",\n" +
                        "            \"isTasFinished\": \"who knows\",\n" +
                        "            \"description\": \"nothing to see\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"test wheels\",\n" +
                        "            \"isTasFinished\": \"154 minutes\",\n" +
                        "            \"description\": \"four wheels\"\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"isTasFinished\": \"no one knows\"\n" +
                        "}"));
        verify(taskUpdateService, times(1))
                .updateTask(any(TaskFormModel.class),any(Long.class));
    }

}