package com.cognizant.persistence.service;

import com.cognizant.model.TaskFormModel;
import com.cognizant.persistence.exception.NoTaskObjectFoundException;
import com.cognizant.persistence.model.SubTask;
import com.cognizant.persistence.model.Task;
import com.cognizant.persistence.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class TaskUpdateService {

    private TaskRepository taskRepository;

    @Autowired
    public TaskUpdateService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * TODO: add aspectj weaving and refactor to private methods
     */
    @Transactional(rollbackFor = NoTaskObjectFoundException.class)
    public void updateTask(TaskFormModel taskFormModel, Long taskId)
            throws NoTaskObjectFoundException {
        Task task = taskRepository.findOneById(taskId)
                .orElseThrow(NoTaskObjectFoundException::new);
        task.setAssignee(taskFormModel.getAssignee());
        task.setIsTasFinished(taskFormModel.getIsTasFinished());
        task.setName(taskFormModel.getName());
        task.getSubTasks().clear();
        taskFormModel.getSubTasks().forEach((s)->{
            SubTask subTask = new SubTask();
            subTask.setDescription(s.getDescription());
            subTask.setIsTasFinished(s.getIsTasFinished());
            subTask.setName(s.getName());
            task.addSubTask(subTask);
        });
        task.setTaskGroup(taskFormModel.getGroup());
        task.setTimeSpent(taskFormModel.getTimeSpent());
        taskRepository.save(task);
    }
}
