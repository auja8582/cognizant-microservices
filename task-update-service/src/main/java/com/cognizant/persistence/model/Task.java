package com.cognizant.persistence.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TASK")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "task", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<SubTask> subTasks = new ArrayList<>();

    private String name;

    @Column(name="TIME_SPENT")
    private String timeSpent;

    @Column(name="TASK_GROUP")
    private String taskGroup;

    private String assignee;

    @Column(name="IS_TAS_FINISHED")
    private String isTasFinished;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<SubTask> getSubTasks() {
        return subTasks;
    }

    public void setSubTasks(List<SubTask> subTasks) {
        this.subTasks = subTasks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(String timeSpent) {
        this.timeSpent = timeSpent;
    }

    public String getTaskGroup() {
        return taskGroup;
    }

    public void setTaskGroup(String taskGroup) {
        this.taskGroup = taskGroup;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getIsTasFinished() {
        return isTasFinished;
    }

    public void setIsTasFinished(String isTasFinished) {
        this.isTasFinished = isTasFinished;
    }

    public void addSubTasks(List<SubTask> subTasks) {
        this.subTasks = subTasks;
        for (SubTask subTask : subTasks) {
            subTask.setTask(this);
        }
    }

    public void addSubTask(SubTask subTask) {
        subTask.setTask(this);
        subTasks.add(subTask);
    }

}
