package com.cognizant.controller;

import com.cognizant.model.TaskFormModel;
import com.cognizant.persistence.exception.NoTaskObjectFoundException;
import com.cognizant.persistence.service.TaskUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TaskUpdateController {

    @Autowired
    private TaskUpdateService taskUpdateService;

    @Autowired
    public TaskUpdateController(TaskUpdateService taskUpdateService) {
        this.taskUpdateService = taskUpdateService;
    }

    @CrossOrigin(origins = "*")
    @PutMapping(value = "/updatetask/{taskId}")
    public ResponseEntity<?> updateTask(@RequestBody TaskFormModel taskFormModel,
                                        @PathVariable Long taskId) {
        try {
            taskUpdateService.updateTask(taskFormModel, taskId);
            return ResponseEntity.ok().build();
        }catch (NoTaskObjectFoundException e){
            return ResponseEntity.notFound().build();
        } catch (Exception e){
            return ResponseEntity.status(500).build();
        }
    }

}