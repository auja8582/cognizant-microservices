package com.cognizant.model;

import java.util.ArrayList;
import java.util.List;

public class TaskFormModel{
    private String name;
    private String timeSpent;
    private String group;
    private String assignee;
    private List<SubTaskFormModel> subTasks = new ArrayList<>();
    private String isTasFinished;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(String timeSpent) {
        this.timeSpent = timeSpent;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public List<SubTaskFormModel> getSubTasks() {
        return subTasks;
    }

    public void setSubTasks(List<SubTaskFormModel> subTasks) {
        this.subTasks = subTasks;
    }

    public String getIsTasFinished() {
        return isTasFinished;
    }

    public void setIsTasFinished(String isTasFinished) {
        this.isTasFinished = isTasFinished;
    }

    @Override
    public String toString() {
        return "TaskFormModel{" +
                "name='" + name + '\'' +
                ", timeSpent='" + timeSpent + '\'' +
                ", group='" + group + '\'' +
                ", assignee='" + assignee + '\'' +
                ", subTasks=" + subTasks +
                ", isTasFinished=" + isTasFinished +
                '}';
    }
}
