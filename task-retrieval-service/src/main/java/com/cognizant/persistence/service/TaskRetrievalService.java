package com.cognizant.persistence.service;

import com.cognizant.model.SubTaskFormModel;
import com.cognizant.model.TaskFormModel;
import com.cognizant.persistence.model.Task;
import com.cognizant.persistence.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
public class TaskRetrievalService {

    @Autowired
    private TaskRepository taskRepository;

    /**
     * TODO: add aspectj weaving and refactor to private methods
     */
    @Transactional
    public Optional<TaskFormModel> getTask(Long taskId) {
        TaskFormModel taskFormModel = convertTaskToTaskFormModel(
                taskRepository.findOneById(taskId));
        return Optional.ofNullable(taskFormModel);
    }

    private TaskFormModel convertTaskToTaskFormModel(Optional<Task> task) {
        return task.map((s) -> {
            TaskFormModel taskFormModel = new TaskFormModel();
            taskFormModel.setAssignee(s.getAssignee());
            taskFormModel.setIsTasFinished(s.getIsTasFinished());
            taskFormModel.setName(s.getName());
            s.getSubTasks().forEach((k) -> {
                SubTaskFormModel subtaskFormModel = new SubTaskFormModel();
                subtaskFormModel.setDescription(k.getDescription());
                subtaskFormModel.setIsTasFinished(k.getIsTasFinished());
                subtaskFormModel.setName(k.getName());
                taskFormModel.getSubTasks().add(subtaskFormModel);
            });
            taskFormModel.setGroup(s.getTaskGroup());
            taskFormModel.setTimeSpent(s.getTimeSpent());
            return taskFormModel;
        }).orElse(null);
    }
}
