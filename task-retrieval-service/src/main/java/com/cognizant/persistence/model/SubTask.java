package com.cognizant.persistence.model;

import javax.persistence.*;

@Entity
@Table(name="SUBTASK")
public class SubTask {

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "TASK_ID", nullable = true)
    private Task task;

    private String name;

    @Column(name="IS_TAS_FINISHED")
    private String isTasFinished;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsTasFinished() {
        return isTasFinished;
    }

    public void setIsTasFinished(String isTasFinished) {
        this.isTasFinished = isTasFinished;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
