package com.cognizant.controller;

import com.cognizant.model.TaskFormModel;
import com.cognizant.persistence.service.TaskRetrievalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class TaskRetrievalController {

    @Autowired
    private TaskRetrievalService taskRetrievalService;

    @Autowired
    public TaskRetrievalController(TaskRetrievalService taskPersistenceService) {
        this.taskRetrievalService = taskPersistenceService;
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/gettask/{taskId}")
    public ResponseEntity<?> getTask(@PathVariable Long taskId) {
        try {
            Optional<TaskFormModel> taskFormModel = taskRetrievalService.getTask(taskId);
            if(taskFormModel.isPresent()){
                return ResponseEntity.ok(taskFormModel);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e){
            return ResponseEntity.status(500).build();
        }
    }

}