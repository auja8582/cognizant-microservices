package com.cognizant.model;

public class SubTaskFormModel {
    private String name;
    private String isTasFinished;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsTasFinished() {
        return isTasFinished;
    }

    public void setIsTasFinished(String isTasFinished) {
        this.isTasFinished = isTasFinished;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "SubTaskFormModel{" +
                "name='" + name + '\'' +
                ", isTasFinished='" + isTasFinished + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
