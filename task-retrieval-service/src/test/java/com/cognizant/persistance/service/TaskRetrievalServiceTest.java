package com.cognizant.persistence.service;

import com.cognizant.TaskRetrievalApplication;
import com.cognizant.controller.TaskRetrievalController;
import com.cognizant.model.TaskFormModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskRetrievalController.class)
@ContextConfiguration(classes = {TaskRetrievalApplication.class})
class TaskRetrievalServiceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskRetrievalController taskRetrievalController;

    @MockBean
    private TaskFormModel taskFormModel;

    @MockBean
    private TaskRetrievalService taskRetrievalService;

    @Test
    public void testTaskRetrievalControllerReturnsNothing() throws Exception {
        when((taskRetrievalService).getTask(1L)).thenReturn(Optional.empty());
        mockMvc.perform(get("/gettask/1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is4xxClientError());
    }

    @Test
    public void testTaskRetrievalControllerReturnsOk() throws Exception {
        when((taskRetrievalService).getTask(1L)).thenReturn(Optional.of(new TaskFormModel()));
        mockMvc.perform(get("/gettask/1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    public void testTaskRetrievalControllerInvokesTaskRetrievalService() throws Exception {
        when((taskRetrievalService).getTask(1L)).thenReturn(Optional.empty());
        mockMvc.perform(get("/gettask/1"));
        verify(taskRetrievalService, times(1))
                .getTask(any(Long.class));
    }

}